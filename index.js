var poema="Arde en tus ojos un misterio, virgen /n esquiva y compañera/nNo sé si es odio o es amor la lumbre/Ninagotable de tu aliaba negra/nConmigo irás mientras proyecte sombra/nmi cuerpo y quede a mi sandalia arena/n¿Eres la sed o el agua en mi camino?-/nDime virgen esquiva y compañera";
var biblica="Antes, en todas estas cosas somos más que vencedores por medio de aquel que nos amó. Por lo cual estoy seguro de que ni la muerte, ni la vida, ni ángeles, ni principados, ni potestades, ni lo presente, ni lo por venir, ni lo alto, ni lo profundo, ni ninguna otra cosa creada nos podrá separar del amor de Dios, que es en Cristo Jesús Señor nuestro."

import  express  from "express";

let app=express();

app.get("/poema",(req,res)=>{

    res.send(poema);
})

app.get("/biblica",(req,res)=>{

    res.send(biblica);
})

app.listen(3000)